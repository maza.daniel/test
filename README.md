# Cobee

## Getting started

This is a base repository to do a pair programming with Cobee tech people :)

## Setup

- Node version: 14.15.0
- Rubt version: 2.7.4
- CocoaPods: 1.11

```
npm install
cd ios/ && pod install && cd ..
npm run ios

```

## Libraries

- Axios
- Jest
- React Navigation 6
- React Native Safe Area Context
- Reactotron
- React Native NetInfo
- React Native Testing Library
- React Native Element (Just for MVP)
- React Native Gesture Handler
- React Native Config (For different envs/schemas)
- Sentry

## Reference for CI/CD

https://github.com/boytpcm123/gitlab-ci-fastlane

https://www.youtube.com/watch?v=Ou6BN3JFvSg
