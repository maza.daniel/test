import * as Screens from '~config/screens';
import { noHeaderOptions } from '~navigators/helpers';
import List from '~screens/List';
import Startup from '~screens/Startup';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

const RootNavigator: React.FC = (): JSX.Element => {
  const { Navigator, Screen } = createNativeStackNavigator();

  return (
    <Navigator initialRouteName={Screens.SCREEN__STARTUP}>
      <Screen component={Startup} name={Screens.SCREEN__STARTUP} options={{ ...noHeaderOptions }} />
      <Screen component={List} name={Screens.SCREEN__LIST} />
    </Navigator>
  );
};

export default RootNavigator;
