import { NativeStackNavigationOptions } from '@react-navigation/native-stack';

export const noHeaderOptions: NativeStackNavigationOptions = {
  headerShown: false,
};
