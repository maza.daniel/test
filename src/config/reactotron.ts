import { NativeModules, Platform } from 'react-native';
import Reactotron from 'reactotron-react-native';

export function ReactotronConfig(): void {
  if (__DEV__ && Reactotron.useReactNative) {
    let host = {};

    if (Platform.OS === 'ios') {
      const scriptURL = NativeModules.SourceCode.scriptURL;
      const scriptHostname = scriptURL.split('://')[1].split(':')[0];

      host = {
        host: scriptHostname,
      };
    }

    // eslint-disable-next-line react-hooks/rules-of-hooks
    Reactotron.useReactNative({
      asyncStorage: false,
      networking: {
        ignoreUrls: /whoami/,
      },
    })
      .configure({
        ...host,
        name: 'Cobee',
      })
      .connect();

    if (Reactotron.clear) {
      Reactotron.clear();
    }
    // eslint-disable-next-line no-console, @typescript-eslint/no-explicit-any
    console.log = Reactotron.log as any;
    // eslint-disable-next-line no-console, @typescript-eslint/no-explicit-any
    console.warn = Reactotron.warn as any;
  }
}
