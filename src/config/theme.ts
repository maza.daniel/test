const myTheme = {
  darkColors: {
    primary: '#121212',
  },
  lightColors: {
    primary: '#f2f2f2',
  },
  mode: 'dark',
};

export default myTheme;
