export interface MagicCard {
  id: string;
  imageUrl: string;
  name: string;
}
