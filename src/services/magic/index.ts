import axios from 'axios';
import { MagicCard } from './types';

const baseUrl = 'https://api.magicthegathering.io/v1';

class MagicService {
  private cards: MagicCard[];

  constructor() {
    this.cards = [];
  }

  async fetch(page: number): Promise<void> {
    try {
      const cards = await axios.get(`${baseUrl}/cards`, { params: { page: page } });

      this.setCards(cards.data.cards);
    } catch (error) {
      throw error;
    }
  }

  getCards(): MagicCard[] {
    return this.cards;
  }

  setCards(cards: MagicCard[]): void {
    this.cards = cards;
  }
}

export default new MagicService();
