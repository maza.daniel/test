import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { fireEvent, render, waitFor } from '@testing-library/react-native';

import RootNavigator from '~navigators/RootNavigator/RootNavigator';

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
jest.mock('@react-native-community/netinfo', () => ({}));

describe('Testing react navigation', () => {
  test('page contains a text', async () => {
    const component = (
      <NavigationContainer>
        <RootNavigator />
      </NavigationContainer>
    );

    const { findByText } = render(component);

    const text = await findByText('Cobee');

    expect(text).toBeTruthy();
  });

  test('clicking on a button list screen', async () => {
    const component = (
      <NavigationContainer>
        <RootNavigator />
      </NavigationContainer>
    );

    const { getByText } = render(component);
    const button = getByText('Press me');

    fireEvent.press(button);

    await waitFor(() => expect(getByText(/List/i)));
  });
});
