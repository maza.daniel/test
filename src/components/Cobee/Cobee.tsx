import RootNavigator from '~navigators/RootNavigator';
import React from 'react';

const Cobee: React.FC = (): JSX.Element => {
  return (
    <>
      <RootNavigator />
    </>
  );
};

export default Cobee;
