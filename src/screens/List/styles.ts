import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    padding: 24,
  },
  footer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
  },
  item: {
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    justifyContent: 'center',
    marginHorizontal: 16,
    marginVertical: 8,
    padding: 20,
  },
  m: {},
  separator: {
    height: 0.5,
    width: '100%',
  },
  title: {
    borderRadius: 6,
    borderWidth: 4,
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 16,
    paddingVertical: 8,
    textAlign: 'center',
  },
});
