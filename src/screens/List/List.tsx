import MagicService from '~services/magic';
import { MagicCard } from '~services/magic/types';
import React, { useCallback, useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, TouchableOpacity, View } from 'react-native';
import { Text } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';

import styles from './styles';

const List: React.FC = (): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState<MagicCard[]>([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    (async function (): Promise<void> {
      getData();
    })();
  }, []);

  const getData = async (): Promise<void> => {
    setLoading(true);
    await MagicService.fetch(page);

    setPage(page + 1);
    setDataSource([...dataSource, ...MagicService.getCards()]);
    setLoading(false);
  };

  const renderFooter = (): JSX.Element => {
    return (
      <View style={styles.footer}>
        <TouchableOpacity activeOpacity={0.9} onPress={getData}>
          <Text>Load More</Text>
          {loading ? <ActivityIndicator color="white" /> : null}
        </TouchableOpacity>
      </View>
    );
  };

  const ItemView = ({ item }: { item: MagicCard }): JSX.Element => {
    return (
      <View style={styles.item}>
        <Text>{item.name}</Text>
      </View>
    );
  };

  const ItemSeparatorView = useCallback((): JSX.Element => {
    return <View style={styles.separator} />;
  }, []);

  const getKey = (_item: MagicCard, index: number): string => {
    return index.toString();
  };

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>List</Text>
      <FlatList
        ItemSeparatorComponent={ItemSeparatorView}
        ListFooterComponent={renderFooter}
        data={dataSource}
        initialNumToRender={8}
        keyExtractor={getKey}
        maxToRenderPerBatch={2}
        renderItem={ItemView}
      />
    </SafeAreaView>
  );
};

export default List;
