import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    padding: 24,
  },
  title: {
    borderRadius: 6,
    borderWidth: 4,
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 16,
    paddingVertical: 8,
    textAlign: 'center',
  },
});
