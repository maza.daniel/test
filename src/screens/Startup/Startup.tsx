import * as Screens from '~config/screens';
import { NetworkContext } from '~contexts/Network';
import React, { useCallback, useContext } from 'react';
import { Alert } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';

import { Props } from './types';
import styles from './styles';

const Startup: React.FC<Props> = ({ navigation }): JSX.Element => {
  const network = useContext(NetworkContext);
  const onPress = useCallback(() => {
    navigation.navigate(Screens.SCREEN__LIST);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <>
        <Text style={styles.title}>Cobee</Text>
        <Button
          icon={{
            color: 'white',
            name: 'arrow-right',
            size: 15,
            type: 'font-awesome',
          }}
          iconRight
          onPress={onPress}
          title="Press me"
        />
        {!network.isConnected && Alert.alert('You are offline')}
      </>
    </SafeAreaView>
  );
};

export default Startup;
