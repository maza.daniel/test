import { RootStackParamList, ScreenNavigationProp } from '~navigators/types';

export interface OwnProps<T extends keyof RootStackParamList> {
  navigation: ScreenNavigationProp<T>;
}

export type Props = OwnProps<'General'>;
