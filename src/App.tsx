import Cobee from '~components/Cobee';
import { NetworkProvider } from '~contexts/Network';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

const App: React.FC = (): JSX.Element => {
  return (
    <SafeAreaProvider>
      <NetworkProvider>
        <Cobee />
      </NetworkProvider>
    </SafeAreaProvider>
  );
};

export default App;
