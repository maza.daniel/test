import NetInfo from '@react-native-community/netinfo';
import React, { ReactNode, useEffect, useState } from 'react';

interface PropsContext {
  isConnected: boolean | null;
}

interface PropsProvider {
  children: ReactNode;
}

const init = {
  isConnected: true,
};

export const NetworkContext = React.createContext<PropsContext>(init);

export const NetworkProvider: React.FC<PropsProvider> = ({ children }): JSX.Element => {
  const [isConnected, setIsConnected] = useState<boolean | null>(true);

  useEffect((): void | (() => void | undefined) => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const connected = state.isConnected;

      setIsConnected(connected);
    });

    return (): void => removeNetInfoSubscription();
  }, []);

  return (
    <NetworkContext.Provider
      value={{
        isConnected: isConnected,
      }}
    >
      {children}
    </NetworkContext.Provider>
  );
};
