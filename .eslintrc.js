module.exports = {
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  plugins: ['@typescript-eslint', 'typescript-sort-keys', 'react-hooks'],
  extends: [
    'plugin:react/recommended', // Uses the recommended rules from @eslint-plugin-react
    'prettier/@typescript-eslint', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    'plugin:prettier/recommended', // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    'plugin:jest/recommended',
    'plugin:react-native/all',
    'plugin:@typescript-eslint/recommended',
  ],
  parserOptions: {
    ecmaVersion: 6, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    ecmaFeatures: {
      jsx: true, // Allows for the parsing of JSX
    },
  },
  rules: {
    // General
    'no-console': 2, // disallow use of console
    'comma-dangle': [1, 'always-multiline'], // allow or disallow trailing commas
    'no-cond-assign': 2, // disallow assignment in conditional expressions
    'no-const-assign': 2, // disallow assignment to const-declared variables
    'no-constant-condition': 2, // disallow use of constant expressions in conditions
    'no-control-regex': 1, // disallow control characters in regular expressions
    'no-debugger': 2, // disallow use of debugger
    'no-dupe-class-members': 2, // Disallow duplicate name in class members
    'no-dupe-keys': 2, // disallow duplicate keys when creating object literals
    'no-empty': 2, // disallow empty statements
    'no-ex-assign': 1, // disallow assigning to the exception in a catch block
    'no-extra-boolean-cast': 1, // disallow double-negation boolean casts in a boolean context
    'no-extra-semi': 1, // disallow unnecessary semicolons
    'no-func-assign': 1, // disallow overwriting functions written as function declarations
    'no-inner-declarations': 1, // disallow function or variable declarations in nested blocks
    'no-invalid-regexp': 1, // disallow invalid regular expression strings in the RegExp constructor
    'no-negated-in-lhs': 1, // disallow negation of the left operand of an in expression
    'no-obj-calls': 1, // disallow the use of object properties of the global object (Math and JSON) as functions
    'no-regex-spaces': 1, // disallow multiple spaces in a regular expression literal
    'no-sparse-arrays': 1, // disallow sparse arrays
    'no-unreachable': 2, // disallow unreachable statements after a return, throw, continue, or break statement
    'use-isnan': 1, // disallow comparisons with the value NaN
    'valid-typeof': 1, // Ensure that the results of typeof are compared against a valid string

    // Best Practices
    // These are rules designed to prevent you from making mistakes. They either prescribe a better way of doing something or help you avoid footguns.
    'block-scoped-var': 0, // treat var statements as if they were block scoped (off by default)
    complexity: 0, // specify the maximum cyclomatic complexity allowed in a program (off by default)
    curly: 1, // specify curly brace conventions for all control statements
    'dot-notation': 1, // encourages use of dot notation whenever possible
    eqeqeq: [1, 'allow-null'], // require the use of === and !==
    'guard-for-in': 0, // make sure for-in loops have an if statement (off by default)
    'no-alert': 1, // disallow the use of alert, confirm, and prompt
    'no-caller': 1, // disallow use of arguments.caller or arguments.callee
    'no-div-regex': 1, // disallow division operators explicitly at beginning of regular expression (off by default)
    'no-else-return': 0, // disallow else after a return in an if (off by default)
    'no-eq-null': 0, // disallow comparisons to null without a type-checking operator (off by default)
    'no-eval': 2, // disallow use of eval()
    'no-extend-native': 1, // disallow adding to native types
    'no-extra-bind': 1, // disallow unnecessary function binding
    'no-fallthrough': 1, // disallow fallthrough of case statements
    'no-floating-decimal': 1, // disallow the use of leading or trailing decimal points in numeric literals (off by default)
    'no-implied-eval': 1, // disallow use of eval()-like methods
    'no-labels': 1, // disallow use of labeled statements
    'no-iterator': 1, // disallow usage of __iterator__ property
    'no-lone-blocks': 1, // disallow unnecessary nested blocks
    'no-loop-func': 0, // disallow creation of functions within loops
    'no-multi-str': 0, // disallow use of multiline strings
    'no-native-reassign': 0, // disallow reassignments of native objects
    'no-new': 1, // disallow use of new operator when not part of the assignment or comparison
    'no-new-func': 2, // disallow use of new operator for Function object
    'no-new-wrappers': 1, // disallows creating new instances of String,Number, and Boolean
    'no-octal': 1, // disallow use of octal literals
    'no-octal-escape': 1, // disallow use of octal escape sequences in string literals, such as var foo = "Copyright \251";
    'no-proto': 1, // disallow usage of __proto__ property
    'no-redeclare': 0, // disallow declaring the same variable more then once
    'no-return-assign': 1, // disallow use of assignment in return statement
    'no-script-url': 1, // disallow use of javascript: urls.
    'no-self-compare': 1, // disallow comparisons where both sides are exactly the same (off by default)
    'no-sequences': 1, // disallow use of comma operator
    'no-unused-expressions': 0, // disallow usage of expressions in statement position
    'no-void': 1, // disallow use of void operator (off by default)
    'no-warning-comments': 0, // disallow usage of configurable warning terms in comments": 1,                        // e.g. TODO or FIXME (off by default)
    'no-with': 1, // disallow use of the with statement
    radix: 1, // require use of the second argument for parseInt() (off by default)
    'semi-spacing': 1, // require a space after a semi-colon
    'vars-on-top': 0, // requires to declare all vars on top of their containing scope (off by default)
    'wrap-iife': 0, // require immediate function invocation to be wrapped in parentheses (off by default)
    yoda: 1, // require or disallow Yoda conditions
    'no-restricted-imports': [2, { patterns: ['../**'] }],

    // Variables
    // These rules have to do with variable declarations.
    'no-catch-shadow': 1, // disallow the catch clause parameter name being the same as a variable in the outer scope (off by default in the node environment)
    'no-delete-var': 1, // disallow deletion of variables
    'no-label-var': 1, // disallow labels that share a name with a variable
    'no-shadow': 0, // disallow declaration of variables already declared in the outer scope
    'no-shadow-restricted-names': 1, // disallow shadowing of names such as arguments
    'no-undef': 2, // disallow use of undeclared variables unless mentioned in a /*global */ block
    'no-undefined': 0, // disallow use of undefined variable (off by default)
    'no-undef-init': 1, // disallow use of undefined when initializing variables
    'no-unused-vars': 0, // disallow declaration of variables that are not used in the code, disabled due to rule to eslint-plugin-typescript/no-unused-vars
    'no-use-before-define': 0, // disallow use of variables before they are defined

    // Stylistic Issues
    // These rules are purely matters of style and are quite subjective.
    'key-spacing': 0,
    'keyword-spacing': 1, // enforce spacing before and after keywords
    'jsx-quotes': [1, 'prefer-double'], // enforces the usage of double quotes for all JSX attribute values which doesn’t contain a double quote
    '@typescript-eslint/explicit-module-boundary-typescomma-spacing': 0,
    'no-multi-spaces': 0,
    'brace-style': 0, // enforce one true brace style (off by default)
    camelcase: 0, // require camel case names
    'consistent-this': 1, // enforces consistent naming when capturing the current execution context (off by default)
    'eol-last': 2, // enforce newline at the end of file, with no multiple empty lines
    'func-names': 0, // require function expressions to have a name (off by default)
    'func-style': 0, // enforces use of function declarations or expressions (off by default)
    'new-cap': 0, // require a capital letter for constructors
    'new-parens': 1, // disallow the omission of parentheses when invoking a constructor with no arguments
    'no-nested-ternary': 1, // disallow nested ternary expressions (off by default)
    'no-array-constructor': 1, // disallow use of the Array constructor
    'no-empty-character-class': 1, // disallow the use of empty character classes in regular expressions
    'no-lonely-if': 0, // disallow if as the only statement in an else block (off by default)
    'no-new-object': 1, // disallow use of the Object constructor
    'no-spaced-func': 2, // disallow space between function identifier and application
    'no-ternary': 0, // disallow the use of ternary operators (off by default)
    'no-trailing-spaces': 2, // disallow trailing whitespace at the end of lines
    'no-underscore-dangle': 0, // disallow dangling underscores in identifiers
    'no-mixed-spaces-and-tabs': 1, // disallow mixed spaces and tabs for indentation
    quotes: [1, 'single', 'avoid-escape'], // specify whether double or single quotes should be used
    'quote-props': 0, // require quotes around object literal property names (off by default)
    semi: 1, // require or disallow use of semicolons instead of ASI
    'sort-vars': 0, // sort variables within the same declaration block (off by default)
    'space-in-brackets': 0, // require or disallow spaces inside brackets (off by default)
    'space-in-parens': 0, // require or disallow spaces inside parentheses (off by default)
    'space-infix-ops': 1, // require spaces around operators
    'space-unary-ops': [1, { words: true, nonwords: false }], // require or disallow spaces before/after unary operators (words on by default, nonwords off by default)
    'max-nested-callbacks': 0, // specify the maximum depth callbacks can be nested (off by default)
    'one-var': 0, // allow just one var statement per function (off by default)
    'wrap-regex': 0, // require regex literals to be wrapped in parentheses (off by default)
    'spaced-comment': [1, 'always'],
    'sort-imports': [1, { ignoreDeclarationSort: true }],
    'prefer-template': 1,
    'prefer-const': 2,
    'max-classes-per-file': [1, 1],
    'sort-keys': [1, 'asc', { caseSensitive: true, natural: false }],
    'padding-line-between-statements': [
      1,
      { blankLine: 'always', prev: ['const', 'let', 'var'], next: '*' },
      {
        blankLine: 'any',
        prev: ['const', 'let', 'var'],
        next: ['const', 'let', 'var'],
      },
    ],

    // Prettier
    // The following rules are made available via `eslint-config-prettier`
    'prettier/prettier': 1,
    'max-len': [1, { code: 120, ignoreUrls: true }],

    // Typescript Plugin
    // The following rules are made available via `@typescript-eslint/eslint-plugin`
    '@typescript-eslint/ban-types': [
      'error',
      {
        types: {
          '{}': false,
          Object: false,
          object: false,
          Function: false,
        },
        extendDefaults: true,
      },
    ],
    '@typescript-eslint/indent': 0,
    '@typescript-eslint/explicit-member-accessibility': 0,
    '@typescript-eslint/camelcase': 0,
    '@typescript-eslint/array-type': [1, { default: 'array-simple' }],
    '@typescript-eslint/no-unused-vars': [
      1,
      {
        vars: 'all',
        args: 'all',
        ignoreRestSiblings: false,
        argsIgnorePattern: '^_',
      },
    ],
    '@typescript-eslint/no-explicit-any': 1,
    '@typescript-eslint/no-use-before-define': 0,
    '@typescript-eslint/consistent-type-definitions': [1, 'interface'],
    '@typescript-eslint/interface-name-prefix': [0, 'always'],
    '@typescript-eslint/no-empty-function': 1,
    '@typescript-eslint/no-empty-interface': 0,
    '@typescript-eslint/no-non-null-assertion': 1,
    '@typescript-eslint/no-inferrable-types': 1,
    '@typescript-eslint/no-shadow': 1,
    '@typescript-eslint/no-this-alias': [1, { allowDestructuring: true }],
    '@typescript-eslint/no-var-requires': 0,
    '@typescript-eslint/explicit-function-return-type': 1,

    // Typescript sort keys plugin
    // The following rules are made available via `eslint-plugin-typescript-sort-keys`
    'typescript-sort-keys/interface': 1,
    'typescript-sort-keys/string-enum': 1,

    // React Plugin
    // The following rules are made available via `eslint-plugin-react`.
    'react/display-name': 0,
    'react/jsx-boolean-value': 0,
    'react/jsx-fragments': 2,
    'react/jsx-no-comment-textnodes': 1,
    'react/jsx-no-duplicate-props': 2,
    'react/jsx-no-undef': 2,
    'react/jsx-sort-props': 1,
    'react/jsx-uses-react': 1,
    'react/jsx-uses-vars': 1,
    'react/no-did-mount-set-state': 1,
    'react/no-did-update-set-state': 1,
    'react/no-multi-comp': 0,
    'react/no-string-refs': 1,
    'react/no-unknown-property': 0,
    'react/prop-types': 0,
    'react/react-in-jsx-scope': 1,
    'react/self-closing-comp': 1,
    'react/wrap-multilines': 0,
    'react/jsx-no-bind': 0,
    'react/sort-comp': [
      1,
      {
        order: [
          'static-methods',
          'static-properties',
          'static-variables',

          'type-annotations',
          'instance-variables',

          'constructor',

          'componentDidMount',
          'shouldComponentUpdate',
          'componentDidUpdate',
          'componentWillUnmount',

          'everything-else',

          '/^get(?!(DerivedStateToProps$)).+$/',
          '/^set(?!(DerivedStateToProps$)).+$/',

          '/^on.+$/',

          '/^render.+$/',
          'render',
        ],
      },
    ],

    // React-Native Plugin
    // The following rules are made available via `eslint-plugin-react-native`
    'react-native/no-unused-styles': 2,
    'react-native/sort-styles': [1, 'asc'],
    'react-native/split-platform-components': 0,
    'react-native/no-inline-styles': 1,
    'react-native/no-raw-text': 0,

    // React Hooks
    'react-hooks/rules-of-hooks': 1,
    'react-hooks/exhaustive-deps': 0,

    // Jest Plugin
    // The following rules are made available via `eslint-plugin-jest`.
    'jest/no-disabled-tests': 2,
    'jest/no-focused-tests': 2,
    'jest/no-identical-title': 1,
    'jest/valid-expect': 2,
    'jest/valid-describe': 0,
  },
  overrides: [
    {
      files: ['**/*.js'],
      rules: {
        // Disable Typescript rules unsupported in JS files
        '@typescript-eslint/explicit-member-accessibility': 'off',
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/camelcase': 'off',
        '@typescript-eslint/array-type': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/consistent-type-definitions': 'off',
        '@typescript-eslint/interface-name-prefix': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/no-empty-interface': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        '@typescript-eslint/no-inferrable-types': 'off',
        '@typescript-eslint/no-this-alias': 'off',
        '@typescript-eslint/no-var-requires': 'off',

        'typescript-sort-keys/interface': 'off',
        'typescript-sort-keys/string-enum': 'off',
      },
    },
  ],
  settings: {
    react: {
      version: 'detect', // Tells eslint-plugin-react to automatically detect the version of React to use
    },
  },
  globals: {
    __DEV__: true,
    __dirname: false,
    __fbBatchedBridgeConfig: false,
    alert: false,
    cancelAnimationFrame: false,
    cancelIdleCallback: false,
    clearImmediate: true,
    clearInterval: false,
    clearTimeout: false,
    console: false,
    document: false,
    escape: false,
    Event: false,
    EventTarget: false,
    exports: false,
    fetch: false,
    FormData: false,
    global: false,
    Map: true,
    module: false,
    navigator: false,
    process: false,
    Promise: true,
    requestAnimationFrame: true,
    requestIdleCallback: true,
    require: false,
    Set: true,
    setImmediate: true,
    setInterval: false,
    setTimeout: false,
    window: false,
    XMLHttpRequest: false,
    JSX: true,
    React: true,
    Headers: true,
    EasingMode: true,
    StyleProp: true,
    Response: true,
    HeadersInit: true,
  },
};
