/* eslint-disable no-console */
const fetch = require('node-fetch');

const TOKEN = process.env.GITHUB_TOKEN;
const BRANCH = process.argv[2];
const MESSAGE = process.argv[3];

addCommentToPR(BRANCH, MESSAGE).catch((error) => {
  console.log(error);
});

async function addCommentToPR(branch, message) {
  const pullRequest = await getPullRequest(branch);
  const issueId = pullRequest.number;

  const response = await fetch(`https://api.gitlab.com/repos/cobee/app/issues/${issueId}/comments`, {
    Accept: 'application/json',
    body: JSON.stringify({ body: message }),
    headers: {
      Accept: 'application/json',
      Authorization: `token ${TOKEN}`,
    },
    method: 'POST',
  });
  const data = await response.json();

  // Logging request response
  console.log(data);
}

async function getPullRequest(branch) {
  const response = await fetch(`https://api.gitlab.com/repos/cobee/app/pulls?head=cobee:${branch}`, {
    Accept: 'application/json',
    headers: {
      Accept: 'application/json',
      Authorization: `token ${TOKEN}`,
    },
    method: 'GET',
  });

  const data = await response.json();

  if (data.length !== 1) {
    throw 'No pull requests matched';
  }
  return data[0];
}
