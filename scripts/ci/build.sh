#!/bin/bash

# Environment variables:
# - ENVIRONMENT [required]
# - BUILD_NUMBER [require]
# Optional
# - BRANCH_NAME [optional] (default=[the current branch])
# - APP_VERSION [optional] (default=[the value from package.json])

# Optional flags:
# - SKIP_SHELL_OUTPUT
# - SKIP_IOS
# - SKIP_ANDROID
# - ONLY_EXPORT (only for iOS)
# - CUSTOM_SHELL_BUILD (needed for building unity shell)
# - CLEAN_ARTIFACTS

# Unity shell variables:
# - SHELL_COMMIT [optional] (default=[the value from .unity-shell-commit])
# - SHELL_BRANCH [optional] (overrides SHELL_COMMIT)
# - SHELL_OUTPUT_PATH [optional] (default="output")
# - SHELL_PLATFORM [optional] (default="all")
# - LESSONS_COMMIT [optional]

# iOS build variables:
# - MODE [required]
# Optional
# - IOS_PROJECT_DIR [optional] (default="ios")
# - IOS_PROJECT_FILE [optional] (default="Unity-iPhone.xcworkspace")
# - ONLY_EXPORT [optional]

# Android build variables:
# - ANDROID_PROJECT [optional] (default="android")
# - ANDROID_BUILD_PERFORMANCE [optional]

set -e -o pipefail

# Functions
error() {
  echo -e "\033[0;31m$1\033[0m"
  exit 1
}

post_comment() {
  if ([ "$CUSTOM_SHELL_BUILD" != true ] && ([ "$CI_JOB_ID" ] || [ "$RUN_ID" ])); then
    node scripts/tools/comment-pr.js $BRANCH_NAME "$1"
  fi
}

export APP_VERSION=${APP_VERSION:-$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')}
export BRANCH_NAME=${BRANCH_NAME:-$(git rev-parse --abbrev-ref HEAD)}


# Parsing named arguments
while :; do
  case $1 in
    ## Required arguments
    -e|--environment)
      if [ "$2" ]; then
        export ENVIRONMENT=$2
        shift
      else
        error "environment argument requires a value"
      fi
    ;;
    -m|--mode)
      if [ "$2" ]; then
        export MODE=$2
        shift
      else
        error "mode argument requires a value"
      fi
    ;;
    -b|--build-number)
      if [ "$2" ]; then
        export BUILD_NUMBER=$2
        shift
      else
        error "build number argument requires a value"
      fi
    ;;

    ## Optional arguments
    -n|--branch-name)
      if [ "$2" ]; then
        export BRANCH_NAME=$2
        shift
      else
        error "branch name argument requires a value"
      fi
    ;;
    -v|--version)
      if [ "$2" ]; then
        export APP_VERSION=$2
        shift
      else
        error "app version argument requires a value"
      fi
    ;;
    --skip-shell-output)
      SKIP_SHELL_OUTPUT=true
    ;;
    --skip-ios)
      SKIP_IOS=true
    ;;
    --skip-android)
      SKIP_ANDROID=true
    ;;
    --only-export)
      export ONLY_EXPORT=true
    ;;
    --custom-shell-build)
      export CUSTOM_SHELL_BUILD=true
    ;;
    --android-build-performance)
      export ANDROID_BUILD_PERFORMANCE=true
    ;;
    --clean-artifacts)
      export CLEAN_ARTIFACTS=true
    ;;
    *) break
  esac
  shift
done

# Checking arguments
if [ -z "$ENVIRONMENT" ]; then
  error "An environment is needed."
fi
if [ "$SKIP_IOS" != true ] && [ -z "$MODE" ]; then
  error "A mode is needed."
fi
if [ -z "$BUILD_NUMBER" ]; then
  error "A build number is needed."
fi

echo
echo -e "== Building \033[0;32m$BRANCH_NAME - $APP_VERSION-$ENVIRONMENT\033[0m ($MODE) =="
if [ "$CUSTOM_SHELL_BUILD" = true ]; then
  SHELL_REF=${SHELL_BRANCH:-$SHELL_COMMIT}
  echo -e "(Custom shell build [shell=\033[0;32m$SHELL_REF\033[0m, lessons=\033[0;32m$LESSONS_COMMIT\033[0m])"
fi
echo

# Setup
time scripts/ci/setup.sh

# Notify build started to the PR

# Gitlab
if [ ! -z "$CI_JOB_ID" ]; then
  post_comment "🏁 <strong>Building started!</strong> 🏁<br/><a href='https://gitlab.com/cobee/app/-/jobs/$CI_JOB_ID'>build: <code>$BUILD_NUMBER</code> <br/> branch: <code>$BRANCH_NAME</code></a>"
fi


# iOS build
if [ "$SKIP_IOS" = true ]; then
  echo
  echo -e "\033[1;33mSkipping iOS build\033[0m"
  echo
else
  scripts/ci/build-ios.sh
fi

# Android build
if [ "$SKIP_ANDROID" = true ]; then
  echo
  echo -e "\033[1;33mSkipping Android build\033[0m"
  echo
else
  scripts/ci/build-android.sh
fi

# Notify build finished to the PR

# Gitlab
if [ ! -z "$CI_JOB_ID" ]; then
  post_comment "🎊 <strong>Builds ready!</strong> 🎊<br/><a href='https://gitlab.com/cobee/app/-/jobs/$CI_JOB_ID/artifacts/browse'>build: <code>$BUILD_NUMBER</code> <br/> branch: <code>$BRANCH_NAME</code></a><br/><code>$APP_VERSION</code> - $ENVIRONMENT ($MODE) [<a href='https://gitlab.com/cobee/app/-/jobs/$CI_JOB_ID/raw'>LOGS</a>]"  
fi
