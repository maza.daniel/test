#!/bin/bash

# Environment variables:
# - ENVIRONMENT [required]
# - BUILD_NUMBER [required]
# - MODE [required]
# - APP_VERSION [optional] (default=[the value from package.json])
# - BRANCH_NAME [optional] (default=[the current branch])
# - IOS_PROJECT_DIR [optional] (default="ios")
# - IOS_PROJECT_FILE [optional] (default="Cobee.xcworkspace")
# - ONLY_EXPORT [optional] 
# - CLEAN_ARTIFACTS [optional]

set -e -o pipefail

APP_VERSION=${APP_VERSION:-$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')}
BRANCH_NAME=${BRANCH_NAME:-$(git rev-parse --abbrev-ref HEAD)}

IOS_PROJECT_DIR=${IOS_PROJECT_DIR:-"ios"}
IOS_PROJECT_FILE=${IOS_PROJECT_FILE:-"Cobee.xcworkspace"}
IOS_PROJECT_PATH="$IOS_PROJECT_DIR/$IOS_PROJECT_FILE"

error() {
  echo -e "\033[0;31m$1\033[0m"
  exit 1
}

# Parsing named arguments
while :; do
  case $1 in
    ## Required arguments
    -e|--environment)
      if [ "$2" ]; then
        ENVIRONMENT=$2
        shift
      else
        error "environment argument requires a value"
      fi
    ;;
    -m|--mode)
      if [ "$2" ]; then
        MODE=$2
        shift
      else
        error "mode argument requires a value"
      fi
    ;;
    -b|--build-number)
      if [ "$2" ]; then
        BUILD_NUMBER=$2
        shift
      else
        error "build number argument requires a value"
      fi
    ;;

    ## Optional arguments
    -n|--branch-name)
      if [ "$2" ]; then
        BRANCH_NAME=$2
        shift
      else
        error "branch name argument requires a value"
      fi
    ;;
    -v|--version)
      if [ "$2" ]; then
        APP_VERSION=$2
        shift
      else
        error "app version argument requires a value"
      fi
    ;;
    --only-export)
      ONLY_EXPORT=true
    ;;
    --clean-artifacts)
      CLEAN_ARTIFACTS=true
    ;;
    *) break
  esac
  shift
done


# Checking arguments
if [ -z "$ENVIRONMENT" ]; then
  error "An environment is needed."
fi
if [ -z "$MODE" ]; then
  error "A mode is needed."
fi
if [ -z "$BUILD_NUMBER" ]; then
  error "A build number is needed."
fi
if [ "$ENVIRONMENT" != "development" ] && [ -z "$SENTRY_AUTH_TOKEN" ]; then
  error "Sentry API key is needed for uploading dSYM file of production/preproduction builds."
fi

INFO_PLIST_PATH="$IOS_PROJECT_DIR/Config/Info.plist"
NOTIFICATION_SERVICE_INFO_PLIST_PATH="$IOS_PROJECT_DIR/notificationservice/Info.plist"
DERIVED_DATA_PATH="$IOS_PROJECT_DIR/build"
ARCHIVE_PATH="$ENVIRONMENT.xcarchive"
EXPORT_PATH="archive"
IPA_NAME=$(echo "Lingokids_iOS_${BRANCH_NAME}_${APP_VERSION}_${BUILD_NUMBER}_${ENVIRONMENT}_${MODE}.ipa" | sed -e 's/\//_/g')
IPA_ROOT_PATH="ipa"
IPA_PATH="$IPA_ROOT_PATH/$ENVIRONMENT/$MODE"

setup() {
  # Metro cache setup (this is needed for preventing conflicts when multiple jobs are building)
  export TMPDIR=$(mktemp -d)

  case $MODE in
    distribution)
      EXPORT_OPTIONS_PLIST='ios/circleci_export_options_distribution.plist'
      ;;
    *)
      EXPORT_OPTIONS_PLIST='ios/circleci_export_options.plist'
      ;;
  esac

  cd ios
  time pod install --repo-update
  cd ..

  if [ "$ONLY_EXPORT" != true ]; then
    fastlane run set_info_plist_value path:"$INFO_PLIST_PATH" key:"CFBundleVersion" value:"$BUILD_NUMBER"
    fastlane run set_info_plist_value path:"$INFO_PLIST_PATH" key:"CFBundleShortVersionString" value:"$APP_VERSION"
    fastlane run set_info_plist_value path:"$NOTIFICATION_SERVICE_INFO_PLIST_PATH" key:"CFBundleVersion" value:"$BUILD_NUMBER"
    fastlane run set_info_plist_value path:"$NOTIFICATION_SERVICE_INFO_PLIST_PATH" key:"CFBundleShortVersionString" value:"$APP_VERSION"
  fi
}

build() {
  echo
  echo -e "Building iOS to \033[1;33m'$IPA_PATH'\033[0m (scheme=\033[0;32m$ENVIRONMENT\033[0m, mode=\033[0;32m$MODE\033[0m, bundleVersion=$BUILD_NUMBER\033[0m, bundleShortVersion=\033[0;32m$APP_VERSION\033[0m)"
  echo

  if [ "$ONLY_EXPORT" != true ]; then
    xcodebuild \
      -workspace $IOS_PROJECT_PATH \
      -destination 'generic/platform=iOS' \
      -scheme $ENVIRONMENT \
      -parallelizeTargets \
      -configuration 'Release' \
      -derivedDataPath $DERIVED_DATA_PATH \
      -archivePath $ARCHIVE_PATH \
      -UseModernBuildSystem=YES \
      archive | xcpretty
  fi

  xcodebuild \
    -exportArchive \
    -exportOptionsPlist $EXPORT_OPTIONS_PLIST \
    -archivePath $ARCHIVE_PATH \
    -exportPath $EXPORT_PATH | xcpretty 

  if [ "$ONLY_EXPORT" != true ]; then
    git checkout $INFO_PLIST_PATH
  fi

  if [ "$CLEAN_ARTIFACTS" = true ]; then
    rm -rf $IPA_ROOT_PATH
  fi

  mkdir -p $IPA_PATH
  if test -f "$EXPORT_PATH/$ENVIRONMENT.ipa"; then
    mv "$EXPORT_PATH/$ENVIRONMENT.ipa" "$IPA_PATH/$IPA_NAME"
  else
    # Note: From XCode 12 onwards, .ipa files are named after CFBundleName, which in our case is ${PRODUCT_NAME}
    # Older versions use the Scheme name instead. 
    mv "$EXPORT_PATH/cobee.ipa" "$IPA_PATH/$IPA_NAME"
  fi

  mkdir -p dSYM
  cp -r $ARCHIVE_PATH/dSYMs/* dSYM
  zip -r $IPA_PATH/dSYM_${IPA_NAME}.zip dSYM
  rm -rf dSYM

  echo
  echo -e "\033[0;32miOS artifacts:"
  ls $IPA_PATH
  echo -e "\033[0m"

  # Upload dSYM (only production and in distribution mode)	
  if [ "$ENVIRONMENT" = "production"  ] && [ "$MODE" = "distribution" ]; then		
    node_modules/@sentry/cli/sentry-cli --auth-token $SENTRY_AUTH_TOKEN upload-dif --org cobee --project cobee-rn-app-production $IPA_PATH/dSYM_${IPA_NAME}.zip
  fi
}

time setup
time build
