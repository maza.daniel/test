#!/bin/bash

# Environment variables:
# - ENVIRONMENT [required]
# - BUILD_NUMBER [required]
# - APP_VERSION [optional] (default=[the value from package.json])
# - BRANCH_NAME [optional] (default=[the current branch])
# - ANDROID_PROJECT [optional] (default="android")
# - ANDROID_BUILD_PERFORMANCE [optional]
# - CLEAN_ARTIFACTS [optional]

set -e -o pipefail

APP_VERSION=${APP_VERSION:-$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')}
BRANCH_NAME=${BRANCH_NAME:-$(git rev-parse --abbrev-ref HEAD)}

ANDROID_PROJECT=${ANDROID_PROJECT:-"android"}

error() {
  echo -e "\033[0;31m$1\033[0m"
  exit 1
}

# Parsing named arguments
while :; do
  case $1 in
    ## Required arguments
    -e|--environment)
      if [ "$2" ]; then
        ENVIRONMENT=$2
        shift
      else
        error "environment argument requires a value"
      fi
    ;;
    -b|--build-number)
      if [ "$2" ]; then
        BUILD_NUMBER=$2
        shift
      else
        error "build number argument requires a value"
      fi
    ;;

    ## Optional arguments
    -n|--branch-name)
      if [ "$2" ]; then
        BRANCH_NAME=$2
        shift
      else
        error "branch name argument requires a value"
      fi
    ;;
    -v|--version)
      if [ "$2" ]; then
        APP_VERSION=$2
        shift
      else
        error "app version argument requires a value"
      fi
    ;;
    --android-build-performance)
      export ANDROID_BUILD_PERFORMANCE=true
    ;;
    --clean-artifacts)
      CLEAN_ARTIFACTS=true
    ;;
    *) break
  esac
  shift
done

# Checking arguments
if [ -z "$ENVIRONMENT" ]; then
  error "An environment is needed."
fi
if [ -z "$BUILD_NUMBER" ]; then
  error "A build number is needed."
fi
if [ -z "$ANDROID_KEYSTORE" ] && [ ! -f "android/app/keystore.keystore" ]; then
  error "An android keystore file is needed."
fi
if [ -z "$ANDROID_KEYSTORE_PROPERTIES" ] && [ ! -f "android/app/keystore.properties" ]; then
  error "An android keystore properties file is needed."
fi

APK_ROOT_PATH="apk"
APK_PATH="$APK_ROOT_PATH/$ENVIRONMENT"
APK_PERFORMANCE_PATH="$APK_PATH/performance"
AAB_PATH="aab/$ENVIRONMENT"

setup() {
  # Metro cache setup (this is needed for preventing conflicts when multiple jobs are building)
  export TMPDIR=$(mktemp -d)

  # Android keystore setup
  if [ ! -f "android/app/keystore.keystore" ]; then
    if [ -f "$ANDROID_KEYSTORE" ]; then
      base64 -D $ANDROID_KEYSTORE -o android/app/keystore.keystore
    else 
      echo $ANDROID_KEYSTORE > keystore_base64
      base64 -D keystore_base64 -o android/app/keystore.keystore
      rm keystore_base64
    fi
  fi
  
  if [ ! -f "android/app/keystore.properties" ]; then
    if [ -f "$ANDROID_KEYSTORE_PROPERTIES" ]; then
      base64 -D $ANDROID_KEYSTORE_PROPERTIES -o android/app/keystore.properties
    else
      echo $ANDROID_KEYSTORE_PROPERTIES > keystore_properties_base64
      base64 -D keystore_properties_base64 -o android/app/keystore.properties
      rm keystore_properties_base64
    fi
  fi

  if [ $ENVIRONMENT == "development" ]; then
    export ENVFILE=".env"
  else
    export ENVFILE=".env.$ENVIRONMENT"
  fi

  # The new android unity shell build requires a local.properties file even if it's empty
  # Normally you could have paths to the ndk and sdk there, or have them configured
  # globally via envvars.
  touch "android/local.properties"
}

build() {
  echo
  echo -e "Building Android to \033[1;33m'$APK_PATH'\033[0m (env=$ENVIRONMENT, versionCode=$BUILD_NUMBER, versionName=$APP_VERSION)"
  echo

  local GRADLE_ARGS="-p $ANDROID_PROJECT --build-cache --max-workers 2 --stacktrace -PversionCode=$BUILD_NUMBER -PversionName=$APP_VERSION -Pbranch=$BRANCH_NAME"

  # Hack to prevent resource duplication issues
  rm -f $ANDROID_PROJECT/src/main/res/drawable-{l,m,xh,xxh,xxxh}dpi/src_*
  
  rm -rf $ANDROID_PROJECT/build/outputs
  
  if [ "$CLEAN_ARTIFACTS" = true ]; then
    rm -rf $APK_ROOT_PATH
  fi

  # Release build
  $ANDROID_PROJECT/gradlew $GRADLE_ARGS app:bundleRelease app:assembleRelease
  mkdir -p $APK_PATH
  mv $ANDROID_PROJECT/build/outputs/apk/release/*.apk $APK_PATH
  mkdir -p $AAB_PATH
  mv $ANDROID_PROJECT/build/outputs/bundle/release/*.aab $AAB_PATH
  
  # Performance build
  if [ "$ANDROID_BUILD_PERFORMANCE" = true ]; then
    $ANDROID_PROJECT/gradlew $GRADLE_ARGS app:assembleReleasePerformance
    mkdir -p $APK_PERFORMANCE_PATH
    mv $ANDROID_PROJECT/build/outputs/apk/releasePerformance/*.apk $APK_PERFORMANCE_PATH
  fi

  echo
  echo -e "\033[0;32mAndroid artifacts:"
  ls $APK_PATH
  echo -e "\033[0m"
}

time setup
time build