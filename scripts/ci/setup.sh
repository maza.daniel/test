#!/bin/bash

# Environment variables:
# - ENVIRONMENT [required]
# - APP_VERSION [optional] (default=[the value from package.json])

set -e -o pipefail

APP_VERSION=${APP_VERSION:-$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')}

error() {
  echo -e "\033[0;31m$1\033[0m"
  exit 1
}

# Parsing named arguments
while :; do
  case $1 in
    -e|--environment)
      if [ "$2" ]; then
        ENVIRONMENT=$2
        shift
      else
        error "environment argument requires a value"
      fi
    ;;
    -v|--app-version)
      if [ "$2" ]; then
        APP_VERSION=$2
        shift
      else
        error "app version argument requires a value"
      fi
    ;;
    *) break
  esac
  shift
done

# Checking arguments
if [ -z "$ENVIRONMENT" ]; then
  error "An environment is needed."
fi

echo
echo -e "Installing dependencies, fetching config and building Typescript => \033[0;32m'$ENVIRONMENT'\033[0m, \033[0;32m'$APP_VERSION'\033[0m"
echo

yarn install
yarn run fetchAssetsForRelease $ENVIRONMENT $APP_VERSION
yarn run build

