import { AppRegistry } from 'react-native';
import App from '~/App';
import { ReactotronConfig } from '~config/reactotron';

ReactotronConfig();

AppRegistry.registerComponent('Cobee', () => App);
